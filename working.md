What should the program do?


# Main Screen:

The main screen has 2 large square buttons, side-to-side.

Button 1:
  * Placement: Left
  * Color: Purple
  * Text: "Check if my tomu is up to date"
  * Text Color: White

Button 2:
  * Placement: Right
  * Color: Orange
  * Text: "Setup a new tomu"
  * Text Color: Orange

On top of the buttons is a banner that reads:

"Welcome to Flash-a-tomu! What do you wish to do?"

## Button 1

Clicking on this button 

1. Check if the existing binaries are up to date

* Can we find a tomu? We need to poll for the U2F and the bootloader's USB IDs.
  - If yes, next step (1.2)
  - If no, poll again
    * Display a message: "Please insert your tomu in a USB port"

1.2 Do we see the U2F or the Bootloader?
  - If U2F, next step (1.3)
  - If Bootloader (1.4)

1.3 Is the U2F up to date?
  - If yes, next step (1.4)
  - If no, ask if we should update it: "The two-factor authentication (2FA)
    software on your tomu is not up to date! Do you want to update it?"
    * If yes, next step (1.3.1)
    * If no, go back to the main screen

1.3.1 Flash the new U2F version
  - Display a message: "Please upload your private key file"
  - Find what's the current counter (using pyu2f?)
  - Flash the new U2F version
  - Inject the private key
  - Save all those, as we might need to redo this process if 1.4.1
  - Display a congratulation message
  - Next step (1.3.2)

1.3.2 Do they want to check if the Bootloader is up to date?
  - Display a message: "Do you also want to check if your tomu's firmware is up
    to date?"
    * If yes, next step (1.3.3)
    * If no, back to the main screen

1.3.3 Help them access the bootloader
  - Display instructions on how to get the bootloader
  - Poll until we see the Bootloader's USB ID
  - Next step (1.4)

1.4 Is the Bootloader up to date?
  - If yes, next step (1.5)
  - If no, ask if we should update it: "The firmware on your tomu is not up to
    date! Do you want to update it?"
    * If yes, next step (1.4.1)
    * If no, go back to the main screen

1.4.1 Flash the new bootloader
  - 

1.5 Everything is up to date
  - Display a message: "Congratulations, your tomu is up to date!"

2. Flash new binaries



